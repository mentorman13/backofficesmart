var express = require('express');
var router = express.Router();
var bdConn = require('../db/DAO');

router.get('/parks/:coords', function (req, res, next) {
    var coords = req.params.coords;
    bdConn.getParks(
        function (rows) { res.send(rows); }
        , next,coords);
});

router.post('/users/login', function (req, res, next) {
    var body = req.body;
    bdConn.postLogin(
        function (rows) { res.send(rows); }
        , next,JSON.stringify(body));
});

router.post('/users/survey', function (req, res, next) {
    var body = req.body;
    console.log(body);
    bdConn.postSurvey(
        function (rows) { res.send(rows); }
        , next,JSON.stringify(body));
});

module.exports = router;