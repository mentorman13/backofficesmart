const { Pool } = require('pg');

const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'SP2',
  password: '123qweASD', // Pedro
  //password: 'a', // David
  port: 5432
});

exports.getParks = function (cb, next,coords) {
  pool.connect((err, client, done) => {
    if (err) throw err
    client.query('select * from v_park_update where ST_Distance_Sphere(geom, ST_MakePoint('+coords+')) <=  + 1500;', (err, res) => {
      done()
      if (err) {
        next(err);
      } else {
        cb(res.rows);
      }
    })
  });
}

exports.postLogin = function (cb, next,body) {
  pool.connect((err, client, done) => {
    if (err) throw err
    var resBody= JSON.parse(body);
    client.query('INSERT INTO users_login (user_id, latitude, longitude) VALUES (\''+resBody.user_id+'\', \''+resBody.latitude+'\', \''+resBody.longitude+'\');', (err, res) => {
      done()
      if (err) {
        next(err);
      } else {
        cb(res.rows);
      }
    })
  });
}

exports.postSurvey = function (cb, next,body) {
  pool.connect((err, client, done) => {
    if (err) throw err
    var resBody= JSON.parse(body);
    console.log(resBody);
    client.query('INSERT INTO aval_park (user_id, id_park, geral) VALUES ('+resBody.user+', '+resBody.park+', '+resBody.geral+');', (err, res) => {
      done()
      if (err) {
        next(err);
      } else {
        cb(res.rows);
      }
    })
  });
}

exports.getMenu = function (cb, next, id_cc) {
  pool.connect((err, client, done) => {
    if (err) throw err
    client.query('select * from v_bo_menu_piso where id_cc = ' + id_cc, (err, res) => {

      done()
      if (err) {
        next(err);
      } else {
        cb(res.rows);
      }
    })
  });
}